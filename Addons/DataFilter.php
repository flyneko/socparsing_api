<?php
final class DataFilter {
    public static function isNumber($value) {
        return preg_match('/^[0-9\\-]*$/', $value);
    }

    public static function isString($value) {
        return preg_match('/^[A-Za-zа-яА-Я\d]+$/ui', $value);
    }
}