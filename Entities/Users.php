<?php

class Users extends BaseEntity {
    public function accessTime() {
        $accessTime = Registry::get('user')['access_time_to'];
        return [
            'state'         => (date('Y-m-d H:i:s') < date($accessTime) ? 'active' : 'inactive'),
            'access_time'   => $accessTime
        ];
    }
}