<?php

class BaseEntity {
    protected $db = null;

    public function __construct(Database $db) {
        $this->db = $db;
        if (file_exists('Exceptions/' . get_class($this) . 'Exceptions.php'))
            require_once 'Exceptions/' . get_class($this) . 'Exceptions.php';
    }
}