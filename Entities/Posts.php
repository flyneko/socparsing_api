<?php
class Posts extends BaseEntity {
    const MAX_COUNT     = 200;
    const DEFAULT_COUNT = 10;

    private function checkUserLink($id, $source) {
        $output = '';

        switch ($source) {
            case 'vk':
                $output = 'https://vk.com/id' . $id;
                break;

            case 'tw':
                $output = 'https://twitter.com/' . $id;
                break;

            case 'fb':
                $output = 'https://www.facebook.com/profile.php?id=' . $id;
                break;

            case 'ok':
                $output = 'http://ok.ru/profile/' . $id;
                break;
        }

        return $output;
    }

    private function includeSearch($query, $condition = []) {
        $count          = self::DEFAULT_COUNT;
        $offset         = 0;
        $inputParams    = $conditions = [];
        $searchParams = [
            'keyword'       => 'string',
            'industry_id'   => 'number',
            'source'        => 'string',
            'country_id'    => 'number',
            'city_id'       => 'number',
            'sex'           => 'number',
            'age_from'      => 'number',
            'age_to'        => 'number',
            'time_post'     => 'string',
            'count'         => 'number',
            'offset'        => 'number'
        ];

        // Filter input data
        foreach ($_REQUEST as $key => $value) {
            if (!in_array($key, array_keys($searchParams)))
                continue;
            if (!call_user_func('DataFilter::is' . ucfirst($searchParams[$key]), $value))
                throw new BaseException(GeneralExceptions::INVALID_ARGUMENT_TYPE, ['argument' => $key, 'type' => $searchParams[$key]]);
        }

        // Check keyword
        if (isset($_REQUEST['keyword']) && !empty($_REQUEST['keyword'])) {
            $maxKeywordLength = 255;
            if (mb_strlen($_REQUEST['keyword']) > $maxKeywordLength)
                throw new BaseException(PostsExceptions::INVALID_KEYWORD_LENGTH, ['length' => $maxKeywordLength]);

            $conditions['keyword']   = 'posts.content LIKE "%' . $_REQUEST['keyword'] . '%"';
        }

        // Check count
        if (isset($_REQUEST['count']) && !empty($_REQUEST['count'])) {
            if ($_REQUEST['count'] > self::MAX_COUNT || $_REQUEST['count'] < 1)
                throw new BaseException(GeneralExceptions::INVALID_ARGUMENT_RANGE, ['argument' => 'count', 'min' => '0', 'max' => self::MAX_COUNT]);
            $count = $_REQUEST['count'];
        }

        // Check offset
        if (isset($_REQUEST['offset']) && !empty($_REQUEST['offset']))
            $offset = $_REQUEST['offset'];

        // Check age from
        if (isset($_REQUEST['age_from']) && !empty($_REQUEST['age_from'])) {
            if ($_REQUEST['age_from'] < 0)
                throw new BaseException(GeneralExceptions::INVALID_ARGUMENT_RANGE, ['argument' => 'age_from', 'min' => '0', 'max' => '99']);

            $conditions['age_from']     = 'age >= :age_from';
            $inputParams['age_from']    = $_REQUEST['age_from'];
        }

        // Check age to
        if (isset($_REQUEST['age_to']) && !empty($_REQUEST['age_to'])) {
            if ($_REQUEST['age_to'] > 99)
                throw new BaseException(GeneralExceptions::INVALID_ARGUMENT_RANGE, ['argument' => 'age_to', 'min' => '0', 'max' => '99']);

            $conditions['age_to']   = 'age <= :age_to';
            $inputParams['age_to']  = $_REQUEST['age_to'];
        }

        // Check age range condition
        if (isset($_REQUEST['age_to']) && !empty($_REQUEST['age_to']) && isset($_REQUEST['age_from']) && !empty($_REQUEST['age_from'])) {
            if ($_REQUEST['age_from'] > $_REQUEST['age_to'])
                throw new BaseException(PostsExceptions::INVALID_AGE_CONDITION);
        }

        // Check sex
        if (isset($_REQUEST['sex']) && !empty($_REQUEST['sex'])) {
            if (!in_array($_REQUEST['sex'], ['0', '1']))
                throw new BaseException(PostsExceptions::UNKNOWN_SEX);
        }

        // Check industry
        if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id'])) {
            $industry = $this->db->single('SELECT id FROM industry WHERE id = :id', ['id' => $_REQUEST['industry_id']]);
            if (empty($industry))
                throw new BaseException(PostsExceptions::INDUSTRY_NOT_EXIST, ['id' => $_REQUEST['industry_id']]);
        }

        // Check source
        if (isset($_REQUEST['source']) && !empty($_REQUEST['source'])) {
            if (!in_array($_REQUEST['source'], ['vk', 'ok', 'fb', 'tw']))
                throw new BaseException(PostsExceptions::UNKNOWN_SOURCE);
        }

        // Check country
        if (isset($_REQUEST['country_id']) && !empty($_REQUEST['country_id'])) {
            $country = $this->db->single('SELECT id FROM _countries WHERE id = :id', ['id' => $_REQUEST['country_id']]);
            if (empty($country))
                throw new BaseException(GeneralExceptions::VARIABLE_NOT_EXIST, ['name' => 'Country', 'id' => $_REQUEST['country_id']]);
        }

        // Check city
        if (isset($_REQUEST['city_id']) && !empty($_REQUEST['city_id'])) {
            $city = $this->db->single('SELECT id FROM _all_cities WHERE id = :id', ['id' => $_REQUEST['city_id']]);
            if (empty($city))
                throw new BaseException(GeneralExceptions::VARIABLE_NOT_EXIST, ['name' => 'City', 'id' => $_REQUEST['city_id']]);
        }

        unset($_REQUEST['count'], $_REQUEST['offset'], $_REQUEST['age_from'], $_REQUEST['age_to'], $_REQUEST['keyword']);

        foreach ($_REQUEST as $key => $value) {
            if (!in_array($key, array_keys($searchParams)))
                continue;
            $conditions[$key]   = 'posts.' . $key . ' = :' . $key;
            $inputParams[$key]  = $value;
        }

        // Check time period
        if (isset($inputParams['time_post'])) {
            switch ($inputParams['time_post']) {
                case 'today':
                    $conditions['time_post'] = 'time_post >= CURDATE()';
                    break;
                case 'yesterday':
                    $conditions['time_post'] = 'time_post >= SUBDATE(CURDATE(), 1) AND time_post < CURDATE()';
                    break;
                case 'week':
                    $conditions['time_post'] = 'time_post >= SUBDATE(CURDATE(), 7)';
                    break;
                case '2weeks':
                    $conditions['time_post'] = 'time_post >= SUBDATE(CURDATE(), 14)';
                    break;
                case 'month':
                    $conditions['time_post'] = 'time_post >= SUBDATE(CURDATE(), 30)';
                    break;

                default:
                    throw new BaseException(PostsExceptions::UNKNOWN_PERIOD);
                    break;
            }
            unset($inputParams['time_post']);
        }

        $conditions = array_merge($conditions, $condition);
        $list = $this->db->query($query . (!empty($conditions) ? ' WHERE ' . implode(' AND ', $conditions) : '') . ' ORDER BY posts.time_post DESC LIMIT ' . $offset . ', ' . $count, $inputParams);

        return [
            'total'     => $this->db->single('SELECT FOUND_ROWS()'),
            'count'     => count($list),
            'posts'     => array_map(function($i) {
                if ($i['city_id'] == '0')
                    unset($i['city_id'], $i['city_name']);

                if ($i['country_id'] == '0')
                    unset($i['country_id'], $i['country_name']);
                return $i;
            }, $list)
        ];
    }

    public function buy() {
        // Filter id
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if (!DataFilter::isNumber($id))
                throw new BaseException(GeneralExceptions::INVALID_ARGUMENT_TYPE, ['argument' => 'ID', 'type' => 'number']);
        } else
            throw new BaseException(GeneralExceptions::ARGUMENT_NOT_PASSED, ['argument' => 'ID']);

        // If post has been bought
        $post = $this->db->single('SELECT id FROM posts_logs WHERE post_id = :post_id AND user_id = :user_id', ['post_id' => $id, 'user_id' => Registry::get('user')['user_id']]);
        if (!empty($post))
            throw new BaseException(PostsExceptions::POST_HAS_PURCHASED, ['id' => $id]);

        // Check post exist
        $post = $this->db->row('SELECT link_post, user_id, source FROM posts WHERE id = :id AND status = "on"', ['id' => $id]);
        if (empty($post))
            throw new BaseException(GeneralExceptions::VARIABLE_NOT_EXIST, ['name' => 'Post', 'id' => $id]);

        $this->db->query('INSERT INTO posts_logs(user_id, post_id, time, status, type_buy) VALUES (:user_id, :post_id, NOW(), "on", "api")', ['post_id' => $id, 'user_id' => Registry::get('user')['user_id']]);
        return [
            'link_post' => $post['link_post'],
            'link_user' => $this->checkUserLink($post['user_id'], $post['source'])
        ];
    }

    public function hide() {
        // Filter id
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if (!DataFilter::isNumber($id))
                throw new BaseException(GeneralExceptions::INVALID_ARGUMENT_TYPE, ['argument' => 'ID', 'type' => 'number']);
        } else
            throw new BaseException(GeneralExceptions::ARGUMENT_NOT_PASSED, ['argument' => 'ID']);

        // If post has been hidden
        $post = $this->db->single('SELECT post_id FROM posts_hidden WHERE post_id = :post_id AND user_id = :user_id', ['post_id' => $id, 'user_id' => Registry::get('user')['user_id']]);
        if (!empty($post))
            throw new BaseException(PostsExceptions::POST_HAS_HIDDEN, ['id' => $id]);

        // Check post exist
        $post = $this->db->single('SELECT id FROM posts WHERE id = :id', ['id' => $id]);
        if (empty($post))
            throw new BaseException(GeneralExceptions::VARIABLE_NOT_EXIST, ['name' => 'Post', 'id' => $id]);

        $this->db->query('INSERT INTO posts_hidden(user_id, post_id) VALUES (:user_id, :post_id)', ['post_id' => $id, 'user_id' => Registry::get('user')['user_id']]);
        return ['hide' => 'success'];
    }

    public function delete() {
        // Filter id
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if (!DataFilter::isNumber($id))
                throw new BaseException(GeneralExceptions::INVALID_ARGUMENT_TYPE, ['argument' => 'ID', 'type' => 'number']);
        } else
            throw new BaseException(GeneralExceptions::ARGUMENT_NOT_PASSED, ['argument' => 'ID']);

        // If post has been deleted
        $post = $this->db->single('SELECT id FROM posts_logs WHERE status = "delete" AND id = :post_id AND user_id = :user_id', ['post_id' => $id, 'user_id' => Registry::get('user')['user_id']]);
        if (!empty($post))
            throw new BaseException(PostsExceptions::POST_HAS_DELETED, ['id' => $id]);

        // If post has not been bought
        $post = $this->db->single('SELECT id FROM posts_logs WHERE id = :post_id AND user_id = :user_id', ['post_id' => $id, 'user_id' => Registry::get('user')['user_id']]);
        if (empty($post))
            throw new BaseException(GeneralExceptions::VARIABLE_NOT_EXIST, ['name' => 'Post', 'id' => $id]);

        $this->db->query('UPDATE posts_logs SET status = "delete" WHERE id = :post_id AND user_id = :user_id', ['post_id' => $id, 'user_id' => Registry::get('user')['user_id']]);
        return ['delete' => 'success'];
    }

    public function purchased() {
        return $this->includeSearch('SELECT SQL_CALC_FOUND_ROWS posts_logs.id, posts.id AS post_id, posts.user_id, link_post, source, posts.industry_id, industry.name AS industry_name, photo_medium, photo_small, sex, age, posts.country_id, _countries.name AS country_name, posts.city_id, cities.city AS city_name,  content, time_post, time_add FROM posts_logs INNER JOIN posts ON posts_logs.post_id = posts.id LEFT JOIN _all_cities AS cities ON cities.id = posts.city_id LEFT JOIN _countries ON _countries.id = posts.country_id INNER JOIN industry ON industry.id = posts.industry_id', ['posts_logs.status <> "delete"', 'posts_logs.user_id = ' . Registry::get('user')['user_id']]);
    }

    public function search() {
        return $this->includeSearch('SELECT SQL_CALC_FOUND_ROWS posts.id, source, posts.industry_id, industry.name AS industry_name, photo_medium, photo_small, sex, age, posts.country_id, _countries.name AS country_name, posts.city_id, cities.city AS city_name, content, time_post, posts.time_add FROM posts LEFT JOIN posts_hidden ON posts.id = posts_hidden.post_id AND posts_hidden.user_id = ' . Registry::get('user')['user_id'] . ' LEFT JOIN posts_logs ON posts.id = posts_logs.post_id AND posts_logs.user_id = ' . Registry::get('user')['user_id'] . ' LEFT JOIN _all_cities AS cities ON cities.id = posts.city_id LEFT JOIN _countries ON _countries.id = posts.country_id INNER JOIN industry ON industry.id = posts.industry_id', ['posts.status = "on"', 'posts_logs.id IS NULL', 'posts_hidden.post_id IS NULL']);
    }
}