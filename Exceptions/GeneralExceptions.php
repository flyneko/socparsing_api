<?php

class GeneralExceptions extends BaseException {
    const TOKEN_WRONG_TYPE          = 'Token has wrong type';
    const TOKEN_NOT_EXIST           = 'Passed token does not exist';
    const TOKEN_BANNED_OWNER        = 'Owner of the token has been banned';
    const TOKEN_INACTIVE_STATUS     = 'Owner of the token has inactive state. Check http://socparsing.ru/access';
    const ARGUMENT_NOT_PASSED       = '{argument} must be pass';
    const ENTITY_NOT_EXIST          = 'Entity does not exist';
    const METHOD_NOT_EXIST          = 'Method does not exist';
    const VARIABLE_NOT_EXIST        = '{name} with ID {id} does not exist';
    const INVALID_ARGUMENT_RANGE    = 'Invalid range of argument {argument}. It can\'t be less than {min} and more than {max}';
    const INVALID_ARGUMENT_TYPE     = 'Invalid argument type. Argument "{argument}" must have {type} type';
    const DATABASE_ERROR            = 'Database error. Try more later';
    const SERVER_ERROR              = 'Server error. Try request more later';
}