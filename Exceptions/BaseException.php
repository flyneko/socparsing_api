<?php
/**
 * Created by PhpStorm.
 * User: shiro
 * Date: 27.6.15
 * Time: 19.16
 */

class BaseException extends Exception {
    public function __construct($message, $replaces = []) {
        foreach ($replaces as $search => $replace) {
            $message = str_ireplace('{' . $search . '}', $replace, $message);
        }
        return parent::__construct($message);
    }
}