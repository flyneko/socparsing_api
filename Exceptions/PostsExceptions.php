<?php

class PostsExceptions extends BaseException {
    const UNKNOWN_SOURCE            = 'Unknown source. It must be one of vk, fb, tw, ok';
    const UNKNOWN_PERIOD            = 'Unknown search period';
    const UNKNOWN_SEX               = 'Unknown sex. It must be 1(female) or 2(male)';
    const POST_HAS_PURCHASED        = 'Post with ID {id} already has been purchased';
    const POST_HAS_HIDDEN           = 'Post with ID {id} already has been hidden';
    const POST_HAS_DELETED          = 'Post with PurchaseID {id} has been deleted';
    const INVALID_AGE_CONDITION     = 'Argument "age_from" can\'t be more than argument "age_to"';
    const INVALID_KEYWORD_LENGTH    = 'Keyword should have length less than {length}';
}