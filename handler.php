<?php
define('ENTITIES', 'Entities');

$entity = ucfirst($_GET['entity']);
$method = $_GET['method'];

// Prepare response
$response = [];
header('Content-Type: application/json; charset=utf-8');

try {
    require_once 'Database/Database.php';
    require_once 'Addons/Registry.php';
    require_once 'Addons/DataFilter.php';

    // Exceptions
    require_once 'Exceptions/BaseException.php';
    require_once 'Exceptions/GeneralExceptions.php';

    $config = require_once 'config.php';

    // Debug or production mode
    if ($config['debug'])
        error_reporting(E_ALL);
    else {
        error_reporting(0);
        set_error_handler(function() { throw new BaseException(GeneralExceptions::SERVER_ERROR); });
    }
    Registry::set('config', $config);

    // Connect to database
    $db = new Database();

    // Check passing of token
    if (isset($_REQUEST['token']) && !empty($_REQUEST['token'])) {
        if (!DataFilter::isString($_REQUEST['token']))
            throw new BaseException(GeneralExceptions::TOKEN_WRONG_TYPE);

        $user = $db->row('SELECT * FROM users WHERE api_key = :token LIMIT 1', ['token' => $_REQUEST['token']]);

        // If token does not exist
        if (empty($user))
            throw new BaseException(GeneralExceptions::TOKEN_NOT_EXIST);

        // If user has been banned
        if ($user['level'] == '-2')
            throw new BaseException(GeneralExceptions::TOKEN_BANNED_OWNER);

        // If user has inactive subscribe
        if ((date('Y-m-d H:i:s') > date($user['access_time_to'])))
            throw new BaseException(GeneralExceptions::TOKEN_INACTIVE_STATUS);

        Registry::set('user', $user);
    } else
        throw new BaseException(GeneralExceptions::TOKEN_NOT_PASSED);

    // Check entity exists
    if (file_exists(ENTITIES . '/' . $entity . '.php')) {
        require_once ENTITIES . '/' . 'BaseEntity.php';
        require_once ENTITIES . '/' . $entity . '.php';
    } else
        throw new BaseException(GeneralExceptions::ENTITY_NOT_EXIST);

    // Check action exists
    if (method_exists($entity, $method) && (new ReflectionMethod($entity, $method))->isPublic()) {
        $response = [
            'status'    => 'success',
            'data'      => call_user_func([new $entity($db), $method])
        ];
    } else
        throw new BaseException(GeneralExceptions::METHOD_NOT_EXIST);

} catch (BaseException $e) {
    $response = [
        'status'    => 'error',
        'message'   => $e->getMessage()
    ];
}

echo stripslashes(json_encode($response, JSON_UNESCAPED_UNICODE));
